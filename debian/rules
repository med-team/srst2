#!/usr/bin/make -f

# DH_VERBOSE := 1

include /usr/share/dpkg/default.mk

%:
	dh $@ --buildsystem=pybuild

override_dh_auto_build:
	dh_auto_build
	markdown README.md > README.html
	markdown database_clustering/README.md > database_clustering_README.html

override_dh_install:
	dh_install
	mv debian/$(DEB_SOURCE)/usr/lib/*/dist-packages/$(DEB_SOURCE)/* debian/$(DEB_SOURCE)/usr/share/$(DEB_SOURCE)
	rm -rf debian/*/usr/bin/*.py debian/$(DEB_SOURCE)/usr/lib/*/dist-packages/
	# fix line endings to make sure Python3 interpreter will be found
	find debian/*/usr/share -name "VFDB*" -exec dos2unix \{\} \;
	sed -i '1s:^#!/usr/local.*python[.0-9]*$$:#!/usr/bin/python3:' debian/$(DEB_SOURCE)/usr/share/$(DEB_SOURCE)/qsub_srst2.py
	find debian -name __pycache__ -type d | xargs rm -rf

override_dh_fixperms:
	dh_fixperms
	chmod +x debian/$(DEB_SOURCE)/usr/share/srst2/database_clustering/get_genus_vfdb.sh
	chmod +x debian/$(DEB_SOURCE)/usr/share/srst2/database_clustering/get_all_vfdb.sh
	chmod -x debian/$(DEB_SOURCE)/usr/share/srst2/database_clustering/align_plot_tree_min3.py
	for py in `find debian -name "*.py"` ; do \
	    if head -n 1 $$py | grep -q '^#! */usr/bin/.*python' ; then chmod +x $$py ; fi ; \
	done

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	cd tests && python3 test_slurm_srst2.py && python3 test_srst2.py
endif

override_dh_installdocs:
	dh_installdocs
	sed -i "s?sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__).*scripts.*?sys.path.append(os.path.abspath('/usr/share/$(DEB_SOURCE)'))?" debian/$(DEB_SOURCE)/usr/share/doc/$(DEB_SOURCE)/tests/*.py
